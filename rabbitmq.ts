import amqp from "amqplib";
import { send_message_to_self } from "./utils";

export const {
  RABBITMQ_URL,
  RABBITMQ_INBOUND_QUEUE = "lineBotInbound",
  RABBITMQ_OUTBOUND_QUEUE = "lineBotOutbound",
} = process.env;

let channel: amqp.Channel | undefined;

const consumeCallback = async (msg: any) => {
  try {
    await send_message_to_self(msg.content.toString());
  } catch (error: any) {
    console.error(error);
  }
};

export async function init() {
  if (!RABBITMQ_URL) {
    console.log(`RABBITMQ_URL not set`);
    return;
  }
  const connection = await amqp.connect(RABBITMQ_URL);
  channel = await connection.createChannel();
  channel.assertQueue(RABBITMQ_INBOUND_QUEUE, {
    durable: false,
  });

  channel.assertQueue(RABBITMQ_OUTBOUND_QUEUE, {
    durable: false,
  });

  // Receiving messages from RabbitMQ, sending them with Line
  channel.consume(RABBITMQ_OUTBOUND_QUEUE, consumeCallback);
}

export const rabbitMqPublish = (message: string) => {
  if (!channel) return;
  channel.sendToQueue(RABBITMQ_INBOUND_QUEUE, Buffer.from(message));
  console.log(
    `Message sent RabbitMQ queue ${RABBITMQ_INBOUND_QUEUE}: ${message}`
  );
};
